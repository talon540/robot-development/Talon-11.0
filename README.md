# Talon-11.0

The 2018 FRC competition 'POWER UP' was an ecstatic and educational experience for countless teams from all over the world. Using the skills of their own members and others, teams were to score as many powercubes into the scale and switches as possible. Cubes could also be placed in the vault to gain an advantage in the playing field. The alliance with the most points would win! 